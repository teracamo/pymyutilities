import vtk
import VTKUtilities.Utilities as utils
import VTKUtilities.ClipCylinder as cylclipper

def main():
    # cube = vtk.vtkCubeSource()
    # cube.SetBounds(-20,20,-20,20,-20,20)
    # cube.Update()
    cube = vtk.vtkSphereSource()
    cube.SetCenter(0,0,0)
    cube.SetRadius(20)
    cube.SetPhiResolution(20)
    cube.SetThetaResolution(120)
    cube.Update()

    trifilter = vtk.vtkTriangleFilter()
    trifilter.SetInputConnection(cube.GetOutputPort())
    trifilter.Update()

    clipper = cylclipper.CylinderClipper()
    clipper.SetInputData(trifilter.GetOutput())
    clipper.SetCenter([0,0,0])
    clipper.SetLength(100)
    clipper.SetNormal([1,0,1])
    clipper.SetRadius(1)
    clipper.SetResolution(50)
    clipper.Update()

    bounds = vtk.vtkFeatureEdges()
    bounds.ColoringOff()
    bounds.SetInputData(clipper.GetOutput())
    bounds.FeatureEdgesOff()
    bounds.Update()

    cubeActor = utils.GetActor(clipper.GetOutput())
    cubeWireFrame = utils.GetActor(clipper.GetOutput())
    cubeWireFrame.GetProperty().SetRepresentationToWireframe()
    cubeWireFrame.GetProperty().SetColor([0,0,1])
    cubeWireFrame.GetProperty().SetLineWidth(2)

    intersectionActor = utils.GetActor(clipper.GetInterSectionPD())
    intersectionActor.GetProperty().SetColor([1,0,0])
    intersectionActor.GetProperty().SetLineWidth(3)

    boundsActor = utils.GetActor(bounds.GetOutput())
    boundsActor.GetProperty().SetColor([1,0,0])
    boundsActor.GetProperty().SetLineWidth(3)

    # renderList = []
    # renderList.append(cubeActor)
    # renderList.append(cubeWireFrame)
    # renderList.append(boundsActor)
    #
    # ren = utils.ActorsRenderer(renderList)
    # ren.Render()


if __name__ == '__main__':
    main()
