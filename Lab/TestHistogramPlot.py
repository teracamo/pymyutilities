import VTKUtilities
import vtk
import matplotlib.pyplot as plt

def GetAreaOfCell(inPD, index):
    c = inPD.GetCell(index)
    p1 = inPD.GetPoint(c.GetPointId(0))
    p2 = inPD.GetPoint(c.GetPointId(1))
    p3 = inPD.GetPoint(c.GetPointId(2))
    return vtk.vtkTriangle.TriangleArea(p1, p2, p3)

def main():
    im = VTKUtilities.utils.Reader3D("../data/ct.nii.gz")
    surface = VTKUtilities.MarchingCube(im, 800)

    con = vtk.vtkConnectivityFilter()
    con.SetInputData(surface)
    con.SetExtractionModeToLargestRegion()
    con.Update()

    geom = vtk.vtkGeometryFilter()
    geom.SetInputConnection(con.GetOutputPort())
    geom.Update()

    smoother = vtk.vtkSmoothPolyDataFilter()
    smoother.SetInputData(surface)
    smoother.SetNumberOfIterations(150)
    smoother.Update()

    surface = smoother.GetOutput()

    arealist = []
    for i in xrange(surface.GetNumberOfCells()):
        arealist.append(GetAreaOfCell(surface, i))

    p = plt.hist(arealist, bins=200)
    plt.show(p)


if __name__ == '__main__':
    main()