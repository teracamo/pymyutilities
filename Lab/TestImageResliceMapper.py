import vtk
import numpy as np
import VTKUtilities.Utilities as utils

def MarchingCube(imData, val):
    resampler = vtk.vtkImageResample()
    resampler.SetAxisMagnificationFactor(0,0.4)
    resampler.SetAxisMagnificationFactor(1,0.4)
    resampler.SetAxisMagnificationFactor(2,0.4)
    resampler.SetInputData(imData)
    resampler.Update()

    mc = vtk.vtkMarchingCubes()
    mc.SetInputConnection(resampler.GetOutputPort())
    mc.ComputeNormalsOff()
    mc.ComputeGradientsOff()
    mc.ComputeScalarsOff()
    mc.SetValue(0, val)
    mc.Update()

    con = vtk.vtkConnectivityFilter()
    con.SetInputConnection(mc.GetOutputPort())
    con.SetExtractionModeToLargestRegion()
    con.Update()

    geom = vtk.vtkGeometryFilter()
    geom.SetInputConnection(con.GetOutputPort())
    geom.Update()

    smoother = vtk.vtkWindowedSincPolyDataFilter()
    smoother.SetInputConnection(geom.GetOutputPort())
    smoother.SetNumberOfIterations(10)
    smoother.BoundarySmoothingOff()
    smoother.FeatureEdgeSmoothingOff()
    smoother.NonManifoldSmoothingOn()
    smoother.NormalizeCoordinatesOn()
    smoother.Update()

    pd = vtk.vtkPolyData()
    pd.DeepCopy(smoother.GetOutput())
    return pd

class observerCallBacks(object):
    def __init__(self, planeWidget, reslicer, renwin):
        starterCoord = reslicer.GetInput().GetCenter()
        self.planeWidget = planeWidget
        self.m_plane = vtk.vtkPlane()
        self.m_plane.SetOrigin(starterCoord)
        self.reslicer = reslicer
        self.renwin = renwin
        self.reslicer.SetSlicePlane(self.m_plane)
        self.planeWidget.AddObserver("InteractionEvent", self.PublicCallBack)
        self.planeWidget.SetOrigin(starterCoord[0], starterCoord[1], starterCoord[2])
        self.planeWidget.SetNormal(0,0,1)
        self.planeWidget.OutlineTranslationOff()

    def PublicCallBack(self, obj, event):
        obj.GetPlane(self.m_plane)



reader = vtk.vtkNIFTIImageReader()
reader.SetFileName("../data/ct.nii.gz")
reader.Update()
im = reader.GetOutput()
skull = MarchingCube(im, 1000)

header = reader.GetNIFTIHeader()
origin = [header.GetQOffsetX(), header.GetQOffsetY(), header.GetQOffsetZ()]

t = vtk.vtkTransform()
t.PostMultiply()
# t.RotateX(45)
# t.Translate(origin)


reslicer = vtk.vtkImageResliceMapper()
reslicer.SetInputData(im)
print reslicer
# reslicer.AutoAdjustImageQualityOff()


# A renderer and render window
renderer = vtk.vtkRenderer()
renderer.SetBackground(0, 0, 1)

renwin = vtk.vtkRenderWindow()
renwin.AddRenderer(renderer)

# An interactor
interactor = vtk.vtkRenderWindowInteractor()
interactor.SetRenderWindow(renwin)
interactor.SetInteractorStyle(vtk.vtkInteractorStyleTrackballCamera())

ip = vtk.vtkImageProperty()
ip.SetColorWindow(2000)
ip.SetColorLevel(1000)
ip.SetAmbient(0.0)
ip.SetDiffuse(1.0)
ip.SetOpacity(1.0)
ip.SetInterpolationTypeToCubic()

ia = vtk.vtkImageSlice()
ia.SetMapper(reslicer)
ia.SetProperty(ip)
ia.SetUserTransform(t)

skullAct = utils.GetActor(skull)
skullAct.GetProperty().SetRepresentationToWireframe()
skullAct.GetProperty().SetOpacity(0.1)
skullAct.SetUserTransform(t)

# A Box widget
planeWidget = vtk.vtkImplicitPlaneWidget()
planeWidget.SetInteractor(interactor)
planeWidget.SetPlaceFactor(1.2)
planeWidget.SetDrawPlane(0)
planeWidget.PlaceWidget(skullAct.GetBounds())
planeWidget.UpdatePlacement()
planeWidget.On()

ob = observerCallBacks(planeWidget, reslicer, renwin)


renderer.AddViewProp(ia)
# renderer.AddActor(skullAct)
renderer.AddActor(utils.GetSphereActor(origin, radius=10))
renderer.AddActor(utils.GetSphereActor([0,0,0],color=[0,1,0], radius=10))
# Start
interactor.Initialize()
interactor.Start()