import VTKUtilities
import vtk

def RepairMesh(inPD):
    pass

def main():
    surface = VTKUtilities.utils.Reader2D("../data/closedCast.stl")

    for i in xrange(surface.GetNumberOfCells()):
        l_cell = surface.GetCell(i)
        l_cellType = l_cell.GetCellType()
        if l_cellType == vtk.VTK_LINE:
            print i
        elif l_cellType != 5:
            print "Cell %i is not triangle, cell type: %i"%(i, l_cellType)

    fE = vtk.vtkFeatureEdges()
    fE.SetInputData(surface)
    fE.FeatureEdgesOff()
    fE.ColoringOn()
    fE.Update()

    edge = fE.GetOutput()

    print edge

    edgeActor = VTKUtilities.utils.GetActor(edge)
    edgeActor.GetProperty().SetColor(1,0,0)
    edgeActor.GetProperty().SetLineWidth(3)
    surfaceActor = VTKUtilities.utils.GetActor(surface)
    surfaceActor.GetProperty().SetColor(0.7,0.7,0)
    surfaceActor.GetProperty().SetRepresentationToWireframe()

    renderList = [edgeActor, surfaceActor]
    ren = VTKUtilities.utils.ActorsRenderer(renderList)
    ren.Render()
    pass

if __name__ == '__main__':
    main()