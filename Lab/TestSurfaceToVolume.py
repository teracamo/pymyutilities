import VTKUtilities.Utilities as utils
import vtk
import numpy as np

def SurfaceToVolume(pd):
    whiteImage = vtk.vtkImageData()
    bounds = pd.GetBounds()
    spacing = np.zeros(3)
    spacing[0] = 0.1
    spacing[1] = 0.1
    spacing[2] = 0.1
    whiteImage.SetSpacing(spacing)

    dim = np.zeros(3, dtype=int)
    for i in xrange(3):
        dim[i] = int(np.ceil((bounds[i * 2 + 1] - bounds[i * 2]) / spacing[i]))
    whiteImage.SetDimensions(dim)
    whiteImage.SetExtent(0, dim[0] - 1, 0, dim[1] - 1, 0, dim[2] - 1)

    origin = np.zeros(3)
    origin[0] = bounds[0] + spacing[0] / 2
    origin[1] = bounds[2] + spacing[1] / 2
    origin[2] = bounds[4] + spacing[2] / 2
    whiteImage.SetOrigin(origin)

    whiteImage.AllocateScalars(vtk.VTK_UNSIGNED_CHAR,1)
    inval = 255
    outval = 0
    count = whiteImage.GetNumberOfPoints()
    for i in xrange(count) :
        whiteImage.GetPointData().GetScalars().SetTuple1(i, inval)

    pol2stenc = vtk.vtkPolyDataToImageStencil()
    pol2stenc.SetInputData(pd)
    pol2stenc.SetOutputOrigin(origin)
    pol2stenc.SetOutputSpacing(spacing)
    pol2stenc.SetOutputWholeExtent(whiteImage.GetExtent())
    pol2stenc.Update()

    imgstenc = vtk.vtkImageStencil()
    imgstenc.SetInputData(whiteImage)
    imgstenc.SetStencilConnection(pol2stenc.GetOutputPort())
    imgstenc.ReverseStencilOff()
    imgstenc.SetBackgroundValue(outval)
    imgstenc.Update()

    return imgstenc.GetOutput()

def main():
    cylinderSource = vtk.vtkCylinderSource()
    cylinderSource.SetResolution(30)
    cylinderSource.SetHeight(150)
    cylinderSource.SetRadius(1)
    cylinderSource.SetCenter(0,0,0)
    cylinderSource.Update()
    pd = vtk.vtkPolyData()
    pd.DeepCopy(cylinderSource.GetOutput())

    cylinderSource.SetRadius(2)
    cylinderSource.Update()
    pd2 = vtk.vtkPolyData()
    pd2.DeepCopy(cylinderSource.GetOutput())

    vol1 = SurfaceToVolume(pd)
    vol2 = SurfaceToVolume(pd2)

    resliceAxes = [1,0,0,0,
                 0,1,0,0,
                 0,0,1,0,
                 0,0,0,1]
    resliceAxesD44 = vtk.vtkMatrix4x4()
    resliceAxesD44.DeepCopy(resliceAxes)
    reslicer = vtk.vtkImageReslice()
    reslicer.SetOutputDimensionality(2)
    reslicer.SetInputData(vol1)
    reslicer.SetResliceAxes(resliceAxesD44)
    reslicer.SetResliceAxesOrigin([0,0,0])
    reslicer.SetInterpolationModeToCubic()
    reslicer.SetAutoCropOutput(1)
    reslicer.Update()

    writer = vtk.vtkNIFTIImageWriter()
    writer.SetFileName("../testoutput/imagestenc.nii.gz")
    writer.SetInputData(vol1)
    writer.Update()
    writer.Write()

    writer.SetFileName("../testoutput/imagestenc2.nii.gz")
    writer.SetInputData(vol2)
    writer.Update()
    writer.Write()

    imageActor = vtk.vtkImageActor()
    imageActor.SetInputData(reslicer.GetOutput())
    imageActor.Update()

    sphereActor = utils.GetActor(cylinderSource.GetOutput())
    sphereActor.GetProperty().SetRepresentationToWireframe()

    l = [imageActor, sphereActor]
    renderer = utils.RenderActors(l)
    renderer.camera.Zoom(0.2)
    renderer.Render()

if __name__ == '__main__':
    main()