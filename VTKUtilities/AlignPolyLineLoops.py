import numpy.linalg as linalg
import numpy as np
import vtk

def GetNeighbors(surface, ptId):
    cellids = vtk.vtkIdList()
    ptidsd = vtk.vtkIdList()
    surface.GetPointCells(ptId, cellids)

    tmpids = []
    for i in xrange(cellids.GetNumberOfIds()):
        surface.GetCellPoints(cellids.GetId(i), ptidsd)
        for j in xrange(ptidsd.GetNumberOfIds()):
            if ptidsd.GetId(j) != ptId:
                tmpids.append(ptidsd.GetId(j))

    return tmpids

def GetNormal(surface, com):
    outNormal = np.array([0,0,0], dtype=float)

    l_looper = 0
    l_preLooper = -1
    while(True):
        nextId = GetNeighbors(surface, l_looper)
        if (nextId[0] != l_preLooper):
            nextId = nextId[0]
        else:
            nextId = nextId[1]

        vect1 = np.array(surface.GetPoint(l_looper)) - com
        vect2 = np.array(surface.GetPoint(nextId)) - com

        print vect1
        print vect2

        outNormal += np.cross(vect1, vect2)
        l_preLooper = l_looper
        l_looper = nextId

        if (l_looper == 0):
            break

        print "looping %s"%l_looper
    outNormal /= linalg.norm(outNormal)
    return outNormal

def AlignPolyLineLoops(pd1, pd2):
    comfilter = vtk.vtkCenterOfMass()

    comfilter.SetInputData(pd1)
    comfilter.Update()
    com1 = np.array(comfilter.GetCenter())

    comfilter.SetInputData(pd2)
    comfilter.Update()
    com2 = np.array(comfilter.GetCenter())

    direction = com1 - com2

    # Get Normal
    norm1 = GetNormal(pd1, com1)
    norm2 = GetNormal(pd2, com2)

    print norm1
    print norm2
    transform = vtk.vtkTransform()

def main():
    reader = vtk.vtkPolyDataReader()
    reader.SetFileName("../data/lowerEdges.vtp")
    reader.Update()

    pd1 = vtk.vtkPolyData()
    pd1.DeepCopy(reader.GetOutput())

    reader.SetFileName("../data/upperEdges.vtp")
    reader.Update()
    pd2 = vtk.vtkPolyData()
    pd2.DeepCopy(reader.GetOutput())


    AlignPolyLineLoops(pd1, pd2)



if __name__ == '__main__':
    main()