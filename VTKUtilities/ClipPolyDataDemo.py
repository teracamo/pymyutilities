import numpy.linalg as linal
import numpy as np
import vtk
import Utilities

def GetIntersector(pd1, pd2):
    tripd1 = vtk.vtkPolyData()
    tripd2 = vtk.vtkPolyData()

    tri = vtk.vtkTriangleFilter()
    tri.SetInputData(pd1)
    tri.Update()
    tripd1.DeepCopy(tri.GetOutput())

    tri.SetInputData(pd2)
    tri.Update()
    tripd2.DeepCopy(tri.GetOutput())


    intersector = vtk.vtkIntersectionPolyDataFilter()
    intersector.SetInputData(0,tripd1)
    intersector.SetInputData(1,tripd2)
    intersector.SplitFirstOutputOn()
    intersector.SplitSecondOutputOn()
    intersector.Update()
    return intersector

def main():
    cast = Utilities.ReaderXML("../data/cast_0.vtp")

    comfilter = vtk.vtkCenterOfMass()
    comfilter.SetInputData(cast)
    comfilter.Update()
    com = np.array([-16, 270, 60])
    comSphere = Utilities.GetSphereActor(com, color=[0,0.5,0.5])


    cly = vtk.vtkCylinderSource()
    cly.SetCenter(0,0,0)
    cly.SetHeight(150)
    cly.SetRadius(1)
    cly.SetResolution(50)
    cly.Update()

    normal = np.array([0.951342, 0.204205, -0.230759])
    normal = normal/linal.norm(normal)
    print linal.norm(normal)
    end1 = normal*150 + np.array(com)
    end2 = -normal*150 + np.array(com)
    endSphere = Utilities.GetSphereActor(end1)
    originalAxis = np.array([0,1,0])
    rotationalAxis = np.cross(originalAxis, -normal)
    anglebisector = (originalAxis + normal)/2
    anglebisector = anglebisector/linal.norm(anglebisector)
    rotationalAngle = np.rad2deg(np.arccos(np.dot(originalAxis, anglebisector)) + np.arccos(np.dot(anglebisector, normal)))

    trans = vtk.vtkTransform()
    trans.PostMultiply()
    trans.Translate(0,75,0)
    trans.RotateWXYZ(rotationalAngle, rotationalAxis)
    trans.Translate(com)
    # trans.Translate(-com[0], -com[1], -com[2])
    transfilter = vtk.vtkTransformFilter()
    transfilter.SetTransform(trans)
    transfilter.SetInputData(cly.GetOutput())
    transfilter.Update()

    # Get Intersector
    intersector = GetIntersector(transfilter.GetOutput(), cast)
    io1 = intersector.GetOutput(0)
    io2 = intersector.GetOutput(1)
    io3 = intersector.GetOutput(2)

    clyclip = vtk.vtkCylinder()
    clyclip.SetCenter(com)
    clyclip.SetRadius(cly.GetRadius()*0.999)
    clyclip.SetTransform(trans.GetLinearInverse())

    clipper = vtk.vtkClipPolyData()
    clipper.SetInputData(io3)
    clipper.SetClipFunction(clyclip)
    clipper.Update()

    Utilities.WritePolyData(clipper.GetOutput(), "../testoutput/test.vtp")

    actor = Utilities.GetActor(clipper.GetOutput())
    surfaceactor = Utilities.GetActor(io3)
    actor.GetProperty().SetColor(0.7,0.3,0)
    actor.GetProperty().SetLineWidth(3)
    actor.GetProperty().SetRepresentationToWireframe()

    arrow0 = Utilities.GetArrowActor(com, com+100*originalAxis)
    arrow1 = Utilities.GetArrowActor(com, end1)
    arrow3 = Utilities.GetArrowActor(com, com-normal*150)
    io2Actor = Utilities.GetActor(io2)
    io2Actor.GetProperty().SetColor(1,0.3,0.3)
    io2Actor.GetProperty().SetRepresentationToWireframe()

    end2Sphere = Utilities.GetSphereActor(end2, color=(0.8,0.8, 0))

    renderlist = [actor, surfaceactor, endSphere, comSphere, arrow0, arrow1, arrow3, end2Sphere]
    # renderlist = [arrow, arrow2]
    renderlist.append(io2Actor)
    Utilities.Render(renderlist)
    pass



if __name__ == '__main__':
    main()
