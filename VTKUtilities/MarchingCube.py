import vtk

def MarchingCube(imData, val):
    resampler = vtk.vtkImageResample()
    resampler.SetAxisMagnificationFactor(0,0.4)
    resampler.SetAxisMagnificationFactor(1,0.4)
    resampler.SetAxisMagnificationFactor(2,0.4)
    resampler.SetInputData(imData)
    resampler.Update()

    mc = vtk.vtkMarchingCubes()
    mc.SetInputConnection(resampler.GetOutputPort())
    mc.ComputeNormalsOff()
    mc.ComputeGradientsOff()
    mc.ComputeScalarsOff()
    mc.SetValue(0, val)
    mc.Update()

    con = vtk.vtkConnectivityFilter()
    con.SetInputConnection(mc.GetOutputPort())
    con.SetExtractionModeToLargestRegion()
    con.Update()

    geom = vtk.vtkGeometryFilter()
    geom.SetInputConnection(con.GetOutputPort())
    geom.Update()

    smoother = vtk.vtkWindowedSincPolyDataFilter()
    smoother.SetInputConnection(geom.GetOutputPort())
    smoother.SetNumberOfIterations(10)
    smoother.BoundarySmoothingOff()
    smoother.FeatureEdgeSmoothingOff()
    smoother.NonManifoldSmoothingOn()
    smoother.NormalizeCoordinatesOn()
    smoother.Update()

    pd = vtk.vtkPolyData()
    pd.DeepCopy(smoother.GetOutput())
    return pd