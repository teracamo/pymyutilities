import vtk


def RemovePt(surface, rmlist):
    """
    Remove point from a surface, also remove all cells that contains the to be removed cell

    :param surface: Input surface
    :param rmlist:  List
    :return:
    """
    newSurface = vtk.vtkPolyData()
    points = vtk.vtkPoints()
    cells = vtk.vtkCellArray()
    for i in xrange(surface.GetNumberOfPoints()):
        if (rmlist.count(i) > 0):
            continue
        else:
            points.InsertNextPoint(surface.GetPoint(i))

    for i in xrange(surface.GetNumberOfCells()):
        tmpcell = surface.GetCell(i)

        # Insert cells
        flag = False
        for j in xrange(tmpcell.GetNumberOfPoints()):
            # if cells contain points that are in remove list, skipp them
            if (rmlist.count(tmpcell.GetPointIds().GetId(j)) > 0):
                flag = True

        if flag:
            continue
        else:
            cells.InsertNextCell(tmpcell)

    newSurface.SetPoints(points)
    newSurface.SetPolys(cells)
    return newSurface

def main():
    rmlist = [374,375,425,426]

    reader = vtk.vtkPolyDataReader()
    reader.SetFileName("lowerEdge.vtp")
    reader.Update()
    surface1 = reader.GetOutput()
    surface1 = RemovePt(surface1, rmlist)

    writer = vtk.vtkPolyDataWriter()
    writer.SetFileName("lowerEdge.vtp")
    writer.SetInputData(surface1)
    writer.Update()
    writer.Write()


if __name__ == '__main__':
    main()