import vtk


def ReorderPolyData(polydata):
    numberOfPoints = polydata.GetNumberOfPoints()
    ptskdtree = vtk.vtkKdTree()
    ptskdtree.BuildLocatorFromPoints(polydata)

    points = vtk.vtkPoints()
    cells = vtk.vtkCellArray()
    l_looper = 0
    l_nextCell = -1
    l_preCell = -1
    while(points.GetNumberOfPoints() < numberOfPoints):
        cellIDs = vtk.vtkIdList()
        ptsIDs = vtk.vtkIdList()
        points.InsertNextPoint(polydata.GetPoint(l_looper))

        polydata.GetPointCells(l_looper, cellIDs)
        if (cellIDs.GetNumberOfIds() > 2):
            print "Warning! Input is not pure polylines.\n"

        for i in xrange(cellIDs.GetNumberOfIds()):
            if (cellIDs.GetId(i) != l_preCell):
                l_nextCell = i
                l_preCell = i
                break;

        polydata.GetCellPoints(l_nextCell, ptsIDs)
        if (ptsIDs.GEtNumberOfIds() > 2):
            print "Warning! Input is not pure polylines.\n"

        for i in xrange(ptsIDs.GEtNumberOfIds()):
            if (ptsIDs.GetId(i) != l_looper):
                l_looper = i

        points.InsertNextPoint(i)

        


    newSurface = vtk.vtkPolyData()
    newSurface.SetPoints(points)
    newSurface.SetPolys(cells)
    return newSurface

def main():
    pass

if __name__ == '__main__':
    main()