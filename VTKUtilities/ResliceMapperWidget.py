import VTKUtilities.Utilities as utils
import vtk


class ResliceMapperWidget(object):
    def __init__(self, inImage):
        self._inImage = inImage
        self._renderer = utils.ActorsRenderer([], [0.4,0.4,0.4])

        # Prepare plane Widget
        self._planeWidget = vtk.vtkImplicitPlaneWidget()
        self._planeWidget.SetInteractor(self._renderer.iren)
        self._planeWidget.SetPlaceFactor(1.2)
        self._planeWidget.SetDrawPlane(0)
        self._planeWidget.PlaceWidget(inImage.GetBounds())
        self._planeWidget.OutlineTranslationOff()
        self._planeWidget.ScaleEnabledOff()
        self._planeWidget.UpdatePlacement()

        # Prepare reslicer
        self._resliceMapper = vtk.vtkImageResliceMapper()
        self._resliceMapper.SetInputData(self._inImage)

        self._reslicer = vtk.vtkImageSlice()
        self._reslicer.SetMapper(self._resliceMapper)

        # Add callback
        self._ob = observerCallBacks(self._planeWidget, self._resliceMapper, self._renderer.renwin)
        self._renderer.ren.AddViewProp(self._reslicer)
        self._renderer.ren.ResetCamera()

    def SetActorRenderer(self, actorRenderer):
        self._planeWidget.SetInteractor(actorRenderer.iren)
        self._ob = observerCallBacks(self._planeWidget, self._resliceMapper, actorRenderer.renwin)
        actorRenderer.ren.AddViewProp(self._reslicer)
        actorRenderer.ren.ResetCamera()

    def Start(self):
        self._renderer.iren.Initialize()
        self._renderer.ren.Render()
        self._planeWidget.On()
        self._renderer.iren.Start()

class observerCallBacks(object):
    def __init__(self, planeWidget, reslicer, renwin):
        starterCoord = reslicer.GetInput().GetCenter()
        self.planeWidget = planeWidget
        self.m_plane = vtk.vtkPlane()
        self.m_plane.SetOrigin(starterCoord)
        self.reslicer = reslicer
        self.renwin = renwin
        self.reslicer.SetSlicePlane(self.m_plane)
        self.planeWidget.AddObserver("InteractionEvent", self.PublicCallBack)
        self.planeWidget.SetOrigin(starterCoord[0], starterCoord[1], starterCoord[2])
        self.planeWidget.SetNormal(0,0,1)
        self.planeWidget.OutlineTranslationOff()


    def PublicCallBack(self, obj, event):
        obj.GetPlane(self.m_plane)
