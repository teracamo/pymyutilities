import numpy as np
import numpy.linalg as linalg
import vtk

def AppendPolyData(list):
    adder = vtk.vtkAppendPolyData()
    for pd in list:
        adder.AddInputData(pd)
    adder.Update()

    cleaner = vtk.vtkCleanPolyData()
    cleaner.SetInputData(adder.GetOutput())
    cleaner.Update()

    return cleaner.GetOutput()

def GetRotationByNormal(originalDirection, targetOrientation):
    originalDirection = np.array(originalDirection)
    targetOrientation = np.array(targetOrientation)

    length = linalg.norm(originalDirection)
    originalDirection = originalDirection/length
    length = linalg.norm(targetOrientation)
    targetOrientation = targetOrientation/length
    bisector = (originalDirection + targetOrientation)/2

    rotateAngle = np.rad2deg(np.arccos(np.dot(originalDirection, bisector)/linalg.norm(bisector))) + np.rad2deg(np.arccos(np.dot(bisector, targetOrientation)/linalg.norm(bisector)))
    rotateAxis = np.cross(originalDirection, targetOrientation)

    transform = vtk.vtkTransform()
    transform.RotateWXYZ(rotateAngle, rotateAxis)
    return transform

def GetArrowActor(start, end, shaftRadius=1., tipRadius=1.5, color=[1,1,1]):
    if len(start) != len(end):
        raise IndexError
        return

    start = np.array(start)
    end = np.array(end)

    arrowSource = vtk.vtkArrowSource()
    arrowSource.SetTipLength(0.1)
    arrowSource.SetShaftResolution(50)
    arrowSource.SetShaftRadius(shaftRadius)
    arrowSource.SetTipRadius(tipRadius)
    arrowSource.Update()

    oriDir = [1,0,0]
    tarDir = end - start
    transform = GetRotationByNormal(oriDir, tarDir)

    transformPD = vtk.vtkTransformPolyDataFilter()
    transformPD.SetTransform(transform)
    transformPD.SetInputData(arrowSource.GetOutput())
    transformPD.Update()

    mapper = vtk.vtkPolyDataMapper()
    mapper.SetInputData(transformPD.GetOutput())

    actor = vtk.vtkActor()
    actor.SetMapper(mapper)
    actor.GetProperty().SetColor(color)
    return actor

def GetSphereActor(point, color = [1,0,0], radius = 3, phiRes=20, thetaRes=20):
    sphere = vtk.vtkSphereSource()
    sphere.SetCenter(point)
    sphere.SetRadius(radius)
    sphere.SetPhiResolution(phiRes)
    sphere.SetThetaResolution(thetaRes)
    sphere.Update()

    actor = GetActor(sphere.GetOutput())
    actor.GetProperty().SetColor(color)
    return actor

def WritePolyDataXML(pd, filename):
    writer = vtk.vtkXMLPolyDataWriter()
    writer.SetFileName(filename)
    writer.SetInputData(pd)
    writer.Update()
    writer.Write()

def Writer3D(filename, vtkImage):
    filetype = filename.split('.')[-1]
    if filetype == "gz":
        filetype = filename.split('.')[-2]

    if filetype == "nii":
        writer = vtk.vtkNIFTIImageWriter()

    writer.SetFileName(filename)
    writer.SetInputData(vtkImage)
    writer.Update()
    writer.Write()


def Reader3D(filename):
    filetype = filename.split('.')[-1]
    if filetype == "gz":
        filetype = filename.split('.')[-2]

    if filetype == "nii":
        reader = vtk.vtkNIFTIImageReader()
        reader.SetFileName(filename)

    reader.Update()
    return reader.GetOutput()

def Reader2D(filename):
    filetype = filename.split('.')[-1]
    if filetype == "vtk" or filetype == "vtp":
        reader = vtk.vtkXMLPolyDataReader()
    elif filetype == "stl":
        reader = vtk.vtkSTLReader()

    reader.SetFileName(filename)
    reader.Update()
    return reader.GetOutput()

def Reader(filename):
    reader = vtk.vtkXMLPolyDataReader()
    reader.SetFileName(filename)
    reader.Update()
    return reader.GetOutput()

def ReaderXML(filename):
    reader = vtk.vtkXMLPolyDataReader()
    reader.SetFileName(filename)
    reader.Update()
    return reader.GetOutput()

def GetActor(polydata):
    mapper = vtk.vtkPolyDataMapper()
    mapper.SetInputData(polydata)
    mapper.Update()

    actor = vtk.vtkActor()
    actor.SetMapper(mapper)

    return actor

class ActorsRenderer():
    def __init__(self, actorList, background=[0,0,0]):
        self.background = background
        self.renwinSize = [800,800]

        self.actorList = actorList
        self.ren = vtk.vtkRenderer()
        self.ren.SetBackground(self.background)
        self.renwin = vtk.vtkRenderWindow()
        self.renwin.SetSize(self.renwinSize)
        self.renwin.AddRenderer(self.ren)
        self.iren = vtk.vtkRenderWindowInteractor()
        self.iren.SetRenderWindow(self.renwin)
        self.iren.SetInteractorStyle(vtk.vtkInteractorStyleTrackballCamera())
        self.camera = self.ren.GetActiveCamera()


    def Render(self):
        for actor in self.actorList:
            self.ren.AddActor(actor)

        self.ren.ResetCamera()
        self.iren.Initialize()
        self.renwin.Render()
        self.iren.Start()



